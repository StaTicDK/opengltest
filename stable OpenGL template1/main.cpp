#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#define numVAOs 1

GLuint renderingProgram;
GLuint vao[numVAOs];

void printShaderLog(GLuint shader) {
	int len = 0;
	int chWrittn = 0;
	char* log;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);

	if (len > 0) {
		log = (char*)malloc(len);
		glGetShaderInfoLog(shader, len, &chWrittn, log);
		cout << "Shader Info Log: " << log << endl;
		free(log);
	}
}

void printProgramLog(int prog) {
	int len = 0;
	int chrWrittn = 0;
	char* log;
	glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &len);

	if (len > 0) {
		log = (char*)malloc(len);
		glGetProgramInfoLog(prog, len, &chrWrittn, log);
		cout << "Program Info Log: " << log << endl;
		free(log);
	}
}

bool checkOpenGLError() {
	bool foundError = false;
	int glErr = glGetError();
	while (glErr != GL_NO_ERROR) {
		cout << "glError: " << glErr << endl;
		foundError = true;
		glErr = glGetError();
	}
	return foundError;
}

string readShaderSource(const char* filePath) {
	string content;
	ifstream fileStream(filePath, ios::in);
	string line = "";

	while (!fileStream.eof()) {
		getline(fileStream, line);
		content.append(line + "\n");
	}
	fileStream.close();
	return content;
}

GLuint createShaderProgram() {
	GLint vertCompiled;
	GLint fragCompiled;
	GLint linked;
	//const char* vshaderSource =
	//	"#version 430    \n"
	//	"void main(void) \n"
	//	"{gl_Position = vec4(0.0, 0.0, 0.0, 1.0);}";
	//
	//const char* fshaderSource =
	//	"#version 430    \n"
	//	"out vec4 color; \n"
	//	"void main(void) \n"
	//	"{ if (gl_FragCoord.x > 300) color = vec4(1.0, 0.0, 0.0, 1.0); else color = vec4(0.0, 0.0, 1.0, 1.0);}";

	string vertShader = readShaderSource("vshader.glsl");
	string fragShader = readShaderSource("fshader.glsl");
	cout << vertShader << endl;
	cout << fragShader << endl;
	
	const char* vertShaderSrc = vertShader.c_str();
	const char* fragShaderSrc = fragShader.c_str();


	GLuint vShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vShader, 1, &vertShaderSrc, NULL);
	
	glShaderSource(fShader, 1, &fragShaderSrc, NULL);
	glCompileShader(vShader);
	checkOpenGLError();
	glGetShaderiv(vShader, GL_COMPILE_STATUS, &vertCompiled);

	if (vertCompiled != 1) {
		cout << "vertex compilation failed" << endl;
		printShaderLog(vShader);
	}

	glCompileShader(fShader);
	checkOpenGLError();
	glGetShaderiv(fShader, GL_COMPILE_STATUS, &fragCompiled);

	if (fragCompiled != 1) {
		cout << "fragment compilation failed" << endl;
		printShaderLog(fShader);
	}
	GLuint vfProgram = glCreateProgram();
	glAttachShader(vfProgram, vShader);
	glAttachShader(vfProgram, fShader);
	glLinkProgram(vfProgram);
	checkOpenGLError();
	glGetProgramiv(vfProgram, GL_LINK_STATUS, &linked);

	if (linked != 1) {
		cout << "linking failed" << endl;
		printProgramLog(vfProgram);
	}

	return vfProgram;
}

void init(GLFWwindow* window) {
	renderingProgram = createShaderProgram();
	glGenVertexArrays(numVAOs, vao);
	glBindVertexArray(vao[0]);
}
void display(GLFWwindow* window, double currentTime) {
	glUseProgram(renderingProgram);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(230.0f);
	glDrawArrays(GL_POINTS, 0, 1);
}
int main(void) {
	if (!glfwInit()) { exit(EXIT_FAILURE); }
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	GLFWwindow* window = glfwCreateWindow(600, 600, "Chapter2 - program1", NULL, NULL);
	glfwMakeContextCurrent(window);
	if (glewInit() != GLEW_OK) { exit(EXIT_FAILURE); }
	glfwSwapInterval(1);
	init(window);
	while (!glfwWindowShouldClose(window)) {
		
		display(window, glfwGetTime());
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

// g++ main.cpp `pkg-config --cflags --libs glew glfw3`
//#include <GL/glew.h>
//#include <GLFW/glfw3.h>
//#include <iostream>
//
//void CheckStatus(GLuint obj, bool isShader)
//{
//    GLint status = GL_FALSE, log[1 << 11] = { 0 };
//    (isShader ? glGetShaderiv : glGetProgramiv)(obj, isShader ? GL_COMPILE_STATUS : GL_LINK_STATUS, &status);
//    if (status == GL_TRUE) return;
//    (isShader ? glGetShaderInfoLog : glGetProgramInfoLog)(obj, sizeof(log), NULL, (GLchar*)log);
//    std::cerr << (GLchar*)log << "\n";
//    std::exit(EXIT_FAILURE);
//}
//
//void AttachShader(GLuint program, GLenum type, const char* src)
//{
//    GLuint shader = glCreateShader(type);
//    glShaderSource(shader, 1, &src, NULL);
//    glCompileShader(shader);
//    CheckStatus(shader, true);
//    glAttachShader(program, shader);
//    glDeleteShader(shader);
//}
//
//const char* const vert = R"GLSL(
//#version 430
//void main()
//{
//    if (gl_VertexID == 0) gl_Position = vec4(0.25, -0.25, 0.0, 1.0);
//    else if (gl_VertexID = 1) gl_Position = vec4(-0.25, -0.25, 0.0, 1.0);
//    else gl_Position = vec4(0.25, 0.25, 0.0, 1.0);
//}
//)GLSL";
//
//const char* const frag = R"GLSL(
//#version 430
//out vec4 color;
//void main()
//{
//    if( gl_FragCoord.x > 300 )
//        color = vec4( 1.0, 0.0, 0.0, 1.0 );
//    else
//        color = vec4( 0.0, 0.0, 1.0, 1.0 );
//}
//)GLSL";
//
//int main(int argc, char** argv)
//{
//    if (!glfwInit())
//    {
//        exit(EXIT_FAILURE);
//    }
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//    GLFWwindow* window = glfwCreateWindow(600, 600, "GLFW", NULL, NULL);
//    glfwMakeContextCurrent(window);
//    if (glewInit() != GLEW_OK)
//    {
//        exit(EXIT_FAILURE);
//    }
//    glfwSwapInterval(1);
//
//    GLuint prog = glCreateProgram();
//    AttachShader(prog, GL_VERTEX_SHADER, vert);
//    AttachShader(prog, GL_FRAGMENT_SHADER, frag);
//    glLinkProgram(prog);
//    CheckStatus(prog, false);
//    glUseProgram(prog);
//
//    GLuint vao = 0;
//    glGenVertexArrays(1, &vao);
//    glBindVertexArray(vao);
//
//    while (!glfwWindowShouldClose(window))
//    {
//        glClearColor(0, 0, 0, 1);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//        glUseProgram(prog);
//        glPointSize(230.0f);
//        glDrawArrays(GL_TRIANGLES, 0, 3);
//        glfwSwapBuffers(window);
//        glfwPollEvents();
//    }
//
//    glfwDestroyWindow(window);
//    glfwTerminate();
//    exit(EXIT_SUCCESS);
//}
